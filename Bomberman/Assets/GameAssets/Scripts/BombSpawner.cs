﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class BombSpawner : NetworkBehaviour {

    public GameObject bomb;
    public GameObject purpleBomb;
    public int firePower = 1;
    public int numberOfBombs = 1;
    public int numberOfPurpleBombs = 3;
    public float fuse = 2;

	void Update () {
        if (!isLocalPlayer)
        {
            return;
        }

        //TODO: make shore we have moved +½ unit before we can drop another bomb.
        //TODO: refactor code.

        if (Input.GetKeyDown(KeyCode.Space) && numberOfBombs >= 1)
        {
            CmdSpawnBomb();
        }

        if (Input.GetKeyDown(KeyCode.LeftShift) && numberOfPurpleBombs >= 1)
        {
            CmdSpawnPurpleBomb();
        }


    }

    [Command]
    private void CmdSpawnBomb()
    {
        Vector2 spawnPos = new Vector2(Mathf.Round(transform.position.x), Mathf.Round(transform.position.y));
        var newBomb = Instantiate(bomb, spawnPos, Quaternion.identity) as GameObject;
        newBomb.GetComponent<Bomb>().firePower = firePower;
        newBomb.GetComponent<Bomb>().fuse = fuse;
        NetworkServer.Spawn(newBomb);
        numberOfBombs--;
        Invoke("AddBomb", fuse);
    }

    [Command]
    private void CmdSpawnPurpleBomb()
    {
        Vector2 spawnPos = new Vector2(Mathf.Round(transform.position.x), Mathf.Round(transform.position.y));
        var newPurpleBomb = Instantiate(purpleBomb, spawnPos, Quaternion.identity) as GameObject;
        newPurpleBomb.GetComponent<PurpleBomb>().firePower = firePower;
        newPurpleBomb.GetComponent<PurpleBomb>().fuse = fuse;
        NetworkServer.Spawn(newPurpleBomb);
        numberOfPurpleBombs--;
    }

    public void AddBomb()
    {
        numberOfBombs++;
    }
}

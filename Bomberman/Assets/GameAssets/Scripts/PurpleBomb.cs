﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class PurpleBomb : NetworkBehaviour
{
    public GameObject skullFire;

    [SyncVar]
    internal int firePower;

    internal float fuse = 2;
    GameController gc;
    Vector3[] directions = new Vector3[] { Vector3.up, Vector3.down, Vector3.left, Vector3.right };

    // Use this for initialization
    void Start()
    {
        Invoke("Explode", fuse);
        gc = GameObject.Find("GameController").GetComponent<GameController>();
    }

    //Bomb should now explode
    public void Explode()
    {
        //Prevent double 
        CancelInvoke("Explode");

        //Create center fire
        Instantiate(skullFire, transform.position, Quaternion.identity);

        //create the rest of the fire
        foreach (var direction in directions)
        {
            SpawnFire(direction);
        }

        //Remove the bomb
        Destroy(gameObject);
    }

    private void SpawnFire(Vector3 offset, int skullFire = 1)
    {
        //Calculte fire position
        int x = (int)transform.position.x + (int)offset.x * skullFire;
        int y = (int)transform.position.y + (int)offset.y * skullFire;

        x = Mathf.Clamp(x, 0, GameController.X - 1);
        y = Mathf.Clamp(y, 0, GameController.Y - 1);

        //If the square is free
        if (gc.level[x, y] == null && skullFire < firePower)
        {
            Instantiate(this.skullFire, transform.position + (offset * skullFire), Quaternion.identity);
            //Call self, keep spawning fire
            SpawnFire(offset, ++skullFire);
        }
        else if (skullFire < firePower)
        {
            //Check if we have hit anything
            if (gc.level[x, y] != null && gc.level[x, y].tag == "Destroyable")
            {
                Instantiate(this.skullFire, transform.position + (offset * skullFire), Quaternion.identity);
            }
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        //Turn on bomb collision when player leaves.
        GetComponent<BoxCollider2D>().isTrigger = false;
    }
}